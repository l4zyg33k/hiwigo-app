package com.gsitm.hiwigo.home;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.gsitm.hiwigo.R;

/**
 * Created by kimjeongin on 2017. 4. 21..
 */

public class Temp3Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.temp3_activity);
        ImageView imageView = (ImageView) findViewById(R.id.imageViewTemp3);
        imageView.setImageResource(R.drawable.tmap);

    }

}
