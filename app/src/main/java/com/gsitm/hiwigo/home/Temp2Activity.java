package com.gsitm.hiwigo.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.gsitm.hiwigo.R;

/**
 * Created by kimjeongin on 2017. 4. 21..
 */

public class Temp2Activity extends Activity {

    private Handler handler;

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(Temp2Activity.this, Temp3Activity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.temp2_activity);

        init();
        ImageView imageView = (ImageView) findViewById(R.id.imageViewTemp2);
        imageView.setImageResource(R.drawable.tmap_setup);

        handler.postDelayed(runnable, 2000);
    }

    public void init() {
        handler = new Handler();
    }
}
