package com.gsitm.hiwigo.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by kimjeongin on 2017. 3. 13..
 */

@Getter
@Setter
public class User {

    private long id;
    private String email;
}
