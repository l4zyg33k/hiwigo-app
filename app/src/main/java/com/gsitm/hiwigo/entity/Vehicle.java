package com.gsitm.hiwigo.entity;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class Vehicle {

	private Long id;

	private String number;	

	private String driver;

	private String mobile;

	public String toString() {
		return number + " / " + driver;
	}
}
