package com.gsitm.hiwigo.entity;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * Spring Security에서 생성한 토큰 담는 클래스
 */
@Getter
@Setter
public class Auth {
    private long id; // 오류시 값 0
    private String token; // 인증토큰
    private Date expireDate; // 인증유효기간
    private Boolean isSuccess; // 성공여부
    private String message;

    public Auth() {
        this.id = 0;
        this.isSuccess = false;
    }

    public Auth(long id, Boolean isSuccess, String message) {
        this.id = id;
        this.isSuccess = isSuccess;
        this.message = message;
    }

    public Auth(long id, String token, Date expireDate, Boolean isSuccess) {
        this.id = id;
        this.token = token;
        this.expireDate = expireDate;
    }

    public Auth(long id, String token, Date expireDate, Boolean isSuccess, String message) {
        this.id = id;
        this.token = token;
        this.expireDate = expireDate;
        this.isSuccess = isSuccess;
        this.message = message;
    }

}
