package com.gsitm.hiwigo.entity;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Route {

	private Long id;

	private String name;

	private Vehicle vehicle;

	public String toString()
	{
		return name;
	}
}
