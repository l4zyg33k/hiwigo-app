package com.gsitm.hiwigo.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Back Office Server(Spring)와 Rest-Request/Response 시 주고받을 기본 메세지 프로토콜
 */
@Getter
@Setter
public class Message {
    private long id;

    private String subject;

    private String text;

    public Message() {
    }

    public Message(long id, String subject, String text) {
        this.id = id;
        this.subject = subject;
        this.text = text;
    }

    public String toString() {
        return "Id:[" + this.getId() + "] Subject:[" + this.getSubject() + "] Text:[" + this.getText() + "]";
    }

}
