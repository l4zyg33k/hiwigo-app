package com.gsitm.hiwigo.entity;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by kimjeongin on 2017. 5. 23..
 */

@Getter
@Setter
public class RoutePin {

    private Long id;

    private String name;

    private String address;

    private Integer turn;

    private BigDecimal longitude;

    private BigDecimal latitude;

    private String arrival;

    private Route route;


    public String toString() {
        return route.getName() + " / " + name;
    }

}
