package com.gsitm.hiwigo.delivery;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gsitm.hiwigo.R;
import com.gsitm.hiwigo.common.RestServiceAgent;
import com.gsitm.hiwigo.common.RestServiceGenerator;
import com.skp.Tmap.TMapView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriveActivity extends AppCompatActivity {

    public static String mApiKey = "9be57668-56c7-30c7-94d0-8c93654f8603"; // 발급받은 appKey
    private Toolbar toolbar;
    private Long routePinId;
    RestServiceAgent restService = RestServiceGenerator.createService(RestServiceAgent.class, null);
    private Button btnStop;
    private Button btnArrival;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_drive);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        LinearLayout layout = (LinearLayout) findViewById(R.id.navigation_map);

        TMapView tmapview = new TMapView(this);
        //tmapview.setSKPMapApiKey(mApiKey);
        tmapview.setLanguage(TMapView.LANGUAGE_KOREAN);
        tmapview.setIconVisibility(true);
        tmapview.setZoomLevel(10);
        tmapview.setMapType(TMapView.MAPTYPE_STANDARD);
        tmapview.setCompassMode(true);
        tmapview.setTrackingMode(true);
        layout.addView(tmapview);

        final Intent intentRcv = getIntent();
        routePinId = intentRcv.getLongExtra("routePinId", 0L);


        btnStop = (Button) findViewById(R.id.btnStop);
        btnStop.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //여기에 이벤트를 적어주세요
                Intent intentSnd = new Intent(DriveActivity.this, VehicleAndRouteActivity.class);
                startActivity(intentSnd);
            }
        });


        btnArrival = (Button) findViewById(R.id.btnArrival);
        btnArrival.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //여기에 이벤트를 적어주세요
                Call<Void> call = restService.arrival(routePinId);
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if(!response.isSuccessful()) {
                            Toast.makeText(getBaseContext(), "서버에 출발 정보를 보내지 못하였습니다.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(getBaseContext(), "서버에 출발 정보를 보내지 못하였습니다.", Toast.LENGTH_SHORT).show();
                    }
                });
                Intent intentSnd = new Intent(DriveActivity.this, VehicleAndRouteActivity.class);
                startActivity(intentSnd);
            }
        });
    }

}


