package com.gsitm.hiwigo.delivery;

import android.content.Context;
import android.widget.Spinner;

/**
 * Created by kimjeongin on 2017. 5. 2..
 */

public interface VehicleAndRouteInteractor {
    void findVehiclesAll(Context c, Spinner w, Spinner w2);

    void findByRouteVehicleId(Context c, Spinner w, long id);
}
