package com.gsitm.hiwigo.delivery;

/**
 * Created by kimjeongin on 2017. 3. 8..
 */

public interface RoutePinView {
    void showProgress();

    void hideProgress();

    void setUnkownError();

    // void findVehiclesAll();

    // void findRoutesByVehicle();

    // void findRoutesAll();
}

