package com.gsitm.hiwigo.delivery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.gsitm.hiwigo.R;
import com.gsitm.hiwigo.entity.Route;
import com.gsitm.hiwigo.tmapusecase.ui.BaseActivity;
/**import com.gsitm.hiwigo.tmapusecase.ui.MainActivity;
*/

/**2017.06.01 ssh t팁연동화면을 점착리스트화면으로 변경*/
import com.gsitm.hiwigo.delivery.RouteStoreActivity;

/**
 * Created by kimjeongin on 2017. 5. 1..
 */

public class VehicleAndRouteActivity extends AppCompatActivity implements VehicleAndRouteView, View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Toolbar toolbar;
    private ProgressBar progressBar;
    private VehicleAndRoutePresenter presenter;
    private Spinner ddlVehicle;
    private Spinner ddlRoutes;
    private Button btnGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_and_route_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        presenter = new VehicleAndRoutePresenterImpl(new VehicleAndRouteInteractorImpl());
        progressBar = (ProgressBar) findViewById(R.id.loginProgressBar);
        ddlVehicle = (Spinner) findViewById(R.id.ddlVehicle);
        ddlRoutes = (Spinner) findViewById(R.id.ddlRoutes);

        ddlVehicle.setOnItemSelectedListener(this);
        ddlRoutes.setOnItemSelectedListener(this);

        btnGo = (Button) findViewById(R.id.btnGo);
        btnGo.setOnClickListener(this);

        presenter.findVehiclesAll(this.getApplicationContext(), ddlVehicle, ddlRoutes);
        // presenter.findByRouteVehicleId(this.getApplicationContext(), ddlRoutes);
    }

    @Override
    public void onClick(View v) {
        // TODO: presenter의 hiwigo 호출
        Intent intent = new Intent(VehicleAndRouteActivity.this, RouteStoreActivity.class);
        Route route = (Route) ddlRoutes.getItemAtPosition(ddlRoutes.getSelectedItemPosition());
        intent.putExtra("routeId", String.valueOf(route.getId()));
        intent.putExtra("routeName", route.getName());
        startActivity(intent);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setUnkownError() {

    }

    /*
    @Override
    public void findVehiclesAll() {
        // TODO: 사용자의 차량리스트 가져오기
    }

    @Override
    public void findRoutesByVehicle() {
        //
    }

    @Override
    public void findRoutesAll() {

    }
    */

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
