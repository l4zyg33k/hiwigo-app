package com.gsitm.hiwigo.delivery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.gsitm.hiwigo.R;
import com.gsitm.hiwigo.common.RestServiceAgent;
import com.gsitm.hiwigo.common.RestServiceGenerator;
import com.gsitm.hiwigo.entity.Message;
import com.gsitm.hiwigo.entity.Route;
import com.gsitm.hiwigo.entity.RoutePin;
import com.gsitm.hiwigo.home.TempActivity;
import com.gsitm.hiwigo.tmapusecase.ui.MainActivity;
import android.widget.Button;

import com.skp.Tmap.TMapTapi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Administrator on 2017-06-02.
 */

public class RouteStoreActivity extends AppCompatActivity implements  View.OnClickListener{


    private Button tmapGo;
    private List<RoutePin> routePins = new ArrayList<RoutePin>();;
    private CustomList adapter;
    private ListView list;
    private HashMap<String, String> pathInfo = new HashMap<String, String>();
    TMapTapi tmaptapi;
    private Intent intentRcv;
    RestServiceAgent restService = RestServiceGenerator.createService(RestServiceAgent.class, null);
    private Toolbar toolbar;

    public static String mApiKey = "9be57668-56c7-30c7-94d0-8c93654f8603"; // 발급받은 appKey
    public static String mBizAppID = "8e3b5bc80c48"; // 발급받은 BizAppID (TMapTapi로 TMap앱 연동을 할 때 BizAppID 꼭 필요)

    @Override
    protected void onCreate(Bundle savedInstaceState) {
        super.onCreate(savedInstaceState);
        setContentView(R.layout.route_store_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tmapGo = (Button) findViewById(R.id.tmapGo);
        tmapGo.setOnClickListener(this);
        list = (ListView) findViewById(R.id.list);
        tmaptapi = new TMapTapi(this);
        tmaptapi.setSKPMapAuthentication(mApiKey);

        intentRcv = getIntent();
        this.findRoutePinByRouteId(Long.parseLong(intentRcv.getStringExtra("routeId")));
    }


    public void onClick(View v) {

        if (!pathInfo.isEmpty()) {

            Call<Void> call = restService.departure(routePins.get(0).getId());
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if(!response.isSuccessful()) {
                        Toast.makeText(getBaseContext(), "서버에 출발 정보를 보내지 못하였습니다.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Toast.makeText(getBaseContext(), "서버에 출발 정보를 보내지 못하였습니다.", Toast.LENGTH_SHORT).show();
                }
            });

            tmaptapi.invokeRoute(pathInfo);

            Intent intentSnd = new Intent(RouteStoreActivity.this, DriveActivity.class);
            //intentSnd.putExtra("routeId", route.getId());
            intentSnd.putExtra("routePinId", routePins.get(routePins.size()-1).getId());
            intentSnd.putExtra("routeName", intentRcv.getStringExtra("routeName"));
            startActivity(intentSnd);
        } else {
            Toast.makeText(getBaseContext(), "배송지점 정보를 가져오지 못하였습니다.", Toast.LENGTH_SHORT).show();
        }

    }

    public class CustomList extends ArrayAdapter<RoutePin> {
        private final Activity context;
        public CustomList(Activity context) {
            super(context, R.layout.route_store_activity, routePins);
            this.context = context;
        }

        @Override
        public int getCount() {
            if (routePins == null || routePins.isEmpty())
            {
                return 0;
            }
            else
            {
                return routePins.size();
            }
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.route_store_activity,null,true);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.image);
            TextView strnm = (TextView) rowView.findViewById(R.id.strnm);
            TextView address = (TextView) rowView.findViewById(R.id.address);
            TextView arrival = (TextView) rowView.findViewById(R.id.arrival);
            strnm.setText(routePins.get(position).getName());
            address.setText(routePins.get(position).getAddress());
            arrival.setText(routePins.get(position).getArrival());

            imageView.setImageResource(R.drawable.logo);
            return rowView;
        }
    }

    public void findRoutePinByRouteId(long id) {

        Call<List<RoutePin>> call = restService.findByRouteId(id);

        // Execute the call asynchronously. Get a positive or negative callback.
        call.enqueue(new Callback<List<RoutePin>>() {
            @Override
            public void onResponse(Call<List<RoutePin>> call, Response<List<RoutePin>> response) {
                // The network call was a success and we got a response
                // TODO: use the repository list and display it
                routePins = (List<RoutePin>) response.body();
                adapter = new CustomList(RouteStoreActivity.this);
                list.setAdapter(adapter);
                adapter.addAll(routePins);
                pathInfo.clear();

                int i = 1;

                for(RoutePin routePin : routePins)
                {
                    if (i != routePins.size()) {
                        pathInfo.put("rV" + String.valueOf(i) + "Name", routePin.getName());
                        pathInfo.put("rV" + String.valueOf(i) + "X", String.valueOf(routePin.getLongitude()));
                        pathInfo.put("rV" + String.valueOf(i) + "Y", String.valueOf(routePin.getLatitude()));
                    }
                    else {
                        pathInfo.put("rGoName", routePin.getName());
                        pathInfo.put("rGoX", String.valueOf(routePin.getLongitude()));
                        pathInfo.put("rGoY", String.valueOf(routePin.getLatitude()));
                    }

                    i++;
                }
            }

            @Override
            public void onFailure(Call<List<RoutePin>> call, Throwable t) {
                // the network call was a failure
                // TODO: handle error
                System.out.println("retrofit failure");
            }
        });
    }
}
