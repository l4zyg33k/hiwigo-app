package com.gsitm.hiwigo.delivery;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.gsitm.hiwigo.R;
import com.gsitm.hiwigo.entity.Route;
import com.gsitm.hiwigo.entity.Vehicle;
import com.gsitm.hiwigo.common.RestServiceAgent;
import com.gsitm.hiwigo.common.RestServiceGenerator;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kimjeongin on 2017. 5. 2..
 *
 * Retrofit 사용법
 * https://futurestud.io/tutorials/retrofit-getting-started-and-android-client
 */

public class VehicleAndRouteInteractorImpl implements VehicleAndRouteInteractor {

    RestServiceAgent restService = RestServiceGenerator.createService(RestServiceAgent.class, null);

    @Override
    public void findVehiclesAll(final Context c, final Spinner w, final Spinner w2) {

        Call<List<Vehicle>> call = restService.findVehicleAll();

        // Execute the call asynchronously. Get a positive or negative callback.
        call.enqueue(new Callback<List<Vehicle>>() {
            @Override
            public void onResponse(Call<List<Vehicle>> call, Response<List<Vehicle>> response) {
                // The network call was a success and we got a response
                // TODO: use the repository list and display it
                ArrayAdapter<Vehicle> dataAdapter = new ArrayAdapter<Vehicle>(c,
                        R.layout.spinner_item, response.body());
                dataAdapter.setDropDownViewResource(R.layout.spinner_item);
                w.setAdapter(dataAdapter);

                w.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        //((TextView)parent.getChildAt(0)).setTextColor(Color.BLACK);

                        Vehicle vehicle = (Vehicle) parent.getItemAtPosition(position);

                        findByRouteVehicleId(c, w2, vehicle.getId());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<Vehicle>> call, Throwable t) {
                // the network call was a failure
                // TODO: handle error
            }
        });
    }

    @Override
    public void findByRouteVehicleId(final Context c, final Spinner w, long id) {

        Call<List<Route>> call = restService.findByRouteVehicleId(id);

        // Execute the call asynchronously. Get a positive or negative callback.
        call.enqueue(new Callback<List<Route>>() {
            @Override
            public void onResponse(Call<List<Route>> call, Response<List<Route>> response) {
                // The network call was a success and we got a response
                // TODO: use the repository list and display it
                ArrayAdapter<Route> dataAdapter = new ArrayAdapter<Route>(c,
                        R.layout.spinner_item, response.body());
                dataAdapter.setDropDownViewResource(R.layout.spinner_item);
                w.setAdapter(dataAdapter);

                w.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        //((TextView)parent.getChildAt(0)).setTextColor(Color.BLACK);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<Route>> call, Throwable t) {
                // the network call was a failure
                // TODO: handle error
            }
        });
    }
}
