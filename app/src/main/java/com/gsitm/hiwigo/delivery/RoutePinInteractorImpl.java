package com.gsitm.hiwigo.delivery;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.gsitm.hiwigo.R;
import com.gsitm.hiwigo.common.RestServiceAgent;
import com.gsitm.hiwigo.common.RestServiceGenerator;
import com.gsitm.hiwigo.entity.Route;
import com.gsitm.hiwigo.entity.RoutePin;
import com.gsitm.hiwigo.entity.Vehicle;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kimjeongin on 2017. 5. 2..
 *
 * Retrofit 사용법
 * https://futurestud.io/tutorials/retrofit-getting-started-and-android-client
 */

public class RoutePinInteractorImpl implements RoutePinInteractor {

    RestServiceAgent restService = RestServiceGenerator.createService(RestServiceAgent.class, null);

    @Override
    public void findRoutePinByRouteId(final Context c, final Spinner w, long id) {

        Call<List<RoutePin>> call = restService.findByRouteId(id);

        // Execute the call asynchronously. Get a positive or negative callback.
        call.enqueue(new Callback<List<RoutePin>>() {
            @Override
            public void onResponse(Call<List<RoutePin>> call, Response<List<RoutePin>> response) {
                // The network call was a success and we got a response
                // TODO: use the repository list and display it
                ArrayAdapter<RoutePin> dataAdapter = new ArrayAdapter<RoutePin>(c,
                        R.layout.spinner_item, response.body());
                dataAdapter.setDropDownViewResource(R.layout.spinner_item);
                w.setAdapter(dataAdapter);

                w.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        //((TextView)parent.getChildAt(0)).setTextColor(Color.BLACK);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<RoutePin>> call, Throwable t) {
                // the network call was a failure
                // TODO: handle error
            }
        });
    }
}
