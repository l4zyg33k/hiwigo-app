package com.gsitm.hiwigo.delivery;

import android.content.Context;
import android.widget.Spinner;

/**
 * Created by kimjeongin on 2017. 5. 1..
 */

public class RoutePinPresenterImpl implements RoutePinPresenter {

    private RoutePinInteractor interactor;

    public RoutePinPresenterImpl(RoutePinInteractor routePinInteractor)
    {
        this.interactor = routePinInteractor;
    }

    @Override
    public void start() {
        return;
    }

    @Override
    public void findRoutePinByRouteId(Context c, Spinner w, long id) {
        interactor.findRoutePinByRouteId(c, w, id);
    }
}
