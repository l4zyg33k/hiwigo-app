package com.gsitm.hiwigo.delivery;

import android.content.Context;
import android.widget.Spinner;

/**
 * Created by kimjeongin on 2017. 5. 1..
 */

public class VehicleAndRoutePresenterImpl implements VehicleAndRoutePresenter {

    private VehicleAndRouteInteractor interactor;

    public VehicleAndRoutePresenterImpl(VehicleAndRouteInteractor vehicleAndRouteInteractor)
    {
        this.interactor = vehicleAndRouteInteractor;
    }

    @Override
    public void start() {
        return;
    }

    @Override
    public void findVehiclesAll(Context c, Spinner w, Spinner w2) {
        interactor.findVehiclesAll(c, w, w2);
    }

    @Override
    public void findByRouteVehicleId(Context c, Spinner w, long id) {
        interactor.findByRouteVehicleId(c, w, id);
    }
}
