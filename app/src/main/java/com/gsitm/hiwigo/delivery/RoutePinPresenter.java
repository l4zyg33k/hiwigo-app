package com.gsitm.hiwigo.delivery;

import android.content.Context;
import android.widget.Spinner;

/**
 * Created by kimjeongin on 2017. 5. 1..
 */

public interface RoutePinPresenter {

    void start();

    // w는 위젯의 약자
    void findRoutePinByRouteId(Context c, Spinner w, long id);
}
