package com.gsitm.hiwigo.delivery;

import android.content.Context;
import android.widget.Spinner;

/**
 * Created by kimjeongin on 2017. 5. 1..
 */

public interface VehicleAndRoutePresenter {

    void start();

    // w는 위젯의 약자
    void findVehiclesAll(Context c, Spinner w, Spinner w2);

    // w는 위젯의 약자
    void findByRouteVehicleId(Context c, Spinner w, long id);
}
