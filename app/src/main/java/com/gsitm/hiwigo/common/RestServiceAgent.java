package com.gsitm.hiwigo.common;

import com.gsitm.hiwigo.entity.Auth;
import com.gsitm.hiwigo.entity.Message;
import com.gsitm.hiwigo.entity.Route;
import com.gsitm.hiwigo.entity.RoutePin;
import com.gsitm.hiwigo.entity.Vehicle;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by kimjeongin on 2017. 3. 12..
 */

public interface RestServiceAgent {

    /**
     * 레트로핏 메서드 추가 방법
     * @[HTTP동사]("URL")
     * Call<type> method();
     */

    @POST("/login/{email}/{password}")
    Call<Auth> login(@Path("email") String email, @Path("password") String password);

    @POST("/test/{myDeviceId}/{request}")
    Call<Message> test(@Path("myDeviceId") String myDeviceId, @Path("request") String request);

    @GET("/api/master/vehicles")
    Call<List<Vehicle>> findVehicleAll();

    @GET("/api/master/routepins")
    Call<List<RoutePin>> findRoutePinAll();

    @GET("/api/master/vehicle/{vehicleId}/routes")
    Call<List<Route>> findByRouteVehicleId(@Path("vehicleId") Long vehicleId);

    @GET("/api/master/route/{routeId}/routepins")
    Call<List<RoutePin>> findByRouteId(@Path("routeId") Long routeId);

    @GET("/api/message/departure/routepin/{routePinId}")
    Call<Void> departure(@Path("routePinId") Long routePinId);

    @GET("/api/message/arrival/routepin/{routePinId}")
    Call<Void> arrival(@Path("routePinId") Long routePinId);


}
