package com.gsitm.hiwigo.login;

/**
 * Created by kimjeongin on 2017. 3. 8..
 */

public interface LoginView {
    void showProgress();

    void hideProgress();

    void setEmailError();

    void setPasswordError();

    void setUnkownError();

    void navigateToHome();
}

