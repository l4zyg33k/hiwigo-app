package com.gsitm.hiwigo.login;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.gsitm.hiwigo.tmapusecase.ui.MainActivity;

/**
 * 로그인 프레젠터구현
 * 뷰로부터 이벤트를 전달받아 모델로 전달한다
 * 뷰제어는 이곳에서만 한다(모델로 뷰를 전달하지 않는다)
 * Created by kimjeongin on 2017. 3. 7..
 */

public class LoginPresenterImpl implements LoginPresenter, LoginInteractor.OnLoginFinishedListener {

    private LoginInteractor loginInteractor;
    private Context context;
    private LoginView loginView;

    public LoginPresenterImpl(LoginView loginView, Context context) {
        this.context = context;
        this.loginView = loginView;
        this.loginInteractor = new LoginInteractorImpl(context);
    }

    /**
     *
     * @param email
     * @param password
     * @return
     */
    @Override
    public void login(String email, String password) {
        if (loginView != null) {
            loginView.showProgress();
        }
        //loginInteractor.loginAsync(email, password, this);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                loginView.hideProgress();
            }
        }, 10000);  // 2000은 2초를 의미합니다.

    }

    @Override
    public boolean isLogin() {
        return loginInteractor.isLogin();
    }

    @Override public void onDestroy() {
        loginView = null;
    }

    @Override public void onEmailError() {
        if (loginView != null) {
            loginView.setEmailError();
            loginView.hideProgress();
        }
    }

    @Override public void onPasswordError() {
        if (loginView != null) {
            loginView.setPasswordError();
            loginView.hideProgress();
        }
    }

    @Override public void onSuccess() {
        if (loginView != null) {
            loginView.navigateToHome();
        }
    }

    @Override
    public void onUnknownError() {
        if (loginView != null) {
            loginView.setUnkownError();
            loginView.hideProgress();
        }
    }
}
