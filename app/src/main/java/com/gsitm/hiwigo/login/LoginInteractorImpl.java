package com.gsitm.hiwigo.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.gsitm.hiwigo.entity.Auth;
import com.gsitm.hiwigo.R;
import com.gsitm.hiwigo.common.RestServiceAgent;
import com.gsitm.hiwigo.common.RestServiceGenerator;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kimjeongin on 2017. 3. 7..
 */

public class LoginInteractorImpl implements LoginInteractor {

    private final Context context;
    private final SharedPreferences sharedPrefs;

    private Pattern pattern;
    private Matcher matcher;
    // 메일 유효성 검사를 위한 정규식 패턴 정의
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public LoginInteractorImpl(Context context) {
        this.context = context;
        this.sharedPrefs = context.getSharedPreferences(context.getString(R.string.shared_preference_name), context.MODE_PRIVATE);
    }

    @Deprecated
    @Override
    public Auth login(String email, String password) {
        if (!isValidEmail(email)) {
            /// todo LoginView에서 오류표시
        }

        RestServiceAgent restService = RestServiceGenerator.createService(RestServiceAgent.class, null);
        Auth auth;

        Call<Auth> call = restService.login(email, password);
        try {
            auth = call.execute().body();
            // SharedPreference 에 인증토큰 추가
            String AuthToken = auth.getToken();
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString("AuthToken", AuthToken);
            editor.commit();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("Error", e.getMessage());
            auth = new Auth();
            auth.setIsSuccess(false);

            // SharedPreference 에서 인증토큰 삭제
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.remove("AuthToken");
            editor.commit();
        }

        return auth;
    }

    public void loginAsync(final String email, final String password, final OnLoginFinishedListener listener)
    {
        RestServiceAgent restLoginService = RestServiceGenerator.createService(RestServiceAgent.class, null);
        Auth auth;
        Call<Auth> call = restLoginService.login(email, password);
        call.enqueue(new Callback<Auth>() {
            Auth result;

            @Override
            public void onResponse(Call<Auth> call, Response<Auth> response) {
                if (response.isSuccessful()) {
                    // user object available
                    result = response.body();
                    Auth auth = response.body();
                    String AuthToken = result.getToken();
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString("AuthToken", AuthToken);
                    editor.commit();
                    listener.onSuccess();
                } else {
                    // error response, no access to resource?
                    result = new Auth();
                    result.setMessage(context.getString(R.string.error_invalid_password));
                    listener.onPasswordError();
                }
            }

            @Override
            public void onFailure(Call<Auth> call, Throwable t) {
                // something went completely south (like no internet connection)
                result = new Auth();
                result.setMessage(context.getString(R.string.error_login_network));
                Log.d("Error", t.getMessage());
                listener.onUnknownError();
            }
        });
    }

    @Override
    public Boolean isLogin() {
        //// TODO: 2017. 3. 7. 로그인되었는지 여부를 확인하는 메서드를 구현해야한다.

        return true;
    }

    @Override
    public boolean isValidEmail(String email) {
        Boolean result = false;

        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        result = matcher.matches();

        return result;
    }
}
