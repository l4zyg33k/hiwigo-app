package com.gsitm.hiwigo.login;

import com.gsitm.hiwigo.entity.Auth;

/**
 * 로그인인터액터(Model)
 * 로그인 관련 비즈니스 로직은 이곳에 있다
 * Created by kimjeongin on 2017. 3. 7..
 */

public interface LoginInteractor {

    public Auth login(String email, String password);
    public void loginAsync(String email, String password, OnLoginFinishedListener listener);
    public Boolean isLogin();
    public boolean isValidEmail(String email);

    interface OnLoginFinishedListener {
        void onEmailError();

        void onPasswordError();

        void onSuccess();

        void onUnknownError();
    }

}
