package com.gsitm.hiwigo.login;

/**
 * Created by kimjeongin on 2017. 3. 7..
 */

public interface LoginPresenter {

    void login(String username, String password);

    boolean isLogin();
    void onDestroy();
}
