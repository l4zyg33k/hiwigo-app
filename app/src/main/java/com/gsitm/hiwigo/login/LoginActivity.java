package com.gsitm.hiwigo.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gsitm.hiwigo.R;
import com.gsitm.hiwigo.delivery.VehicleAndRouteActivity;
import com.gsitm.hiwigo.tmapusecase.ui.MainActivity;

public class LoginActivity extends AppCompatActivity implements LoginView, View.OnClickListener {

    private LoginPresenter presenter;
    private Toolbar toolbar;
    private TextView messageText;
    private ProgressBar progressBar;
    private EditText email;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        presenter = new LoginPresenterImpl(this, this.getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        messageText = (TextView) findViewById(R.id.messageText);
        progressBar = (ProgressBar) findViewById(R.id.loginProgressBar);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        findViewById(R.id.loginButton).setOnClickListener(this);

        password.requestFocus();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setEmailError() {
        email.setError(getString(R.string.error_invalid_email));
    }

    @Override
    public void setPasswordError() {
        password.setError(getString(R.string.error_invalid_password));
    }

    @Override
    public void setUnkownError() {
        messageText.setError(getString(R.string.error_unknown));
    }

    @Override
    public void navigateToHome() {
        ///// TODO: 2017. 3. 8. 로그인 성공시 홈 엑비비티 실행
    }

    @Override
    public void onClick(View v) {
        presenter.login(email.getText().toString(), password.getText().toString());

        Intent intent = new Intent(this, VehicleAndRouteActivity.class);
        startActivity(intent);

    }
}
