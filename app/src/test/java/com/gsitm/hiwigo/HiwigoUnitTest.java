package com.gsitm.hiwigo;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.gsitm.hiwigo.entity.Message;
import com.gsitm.hiwigo.common.RestServiceAgent;
import com.gsitm.hiwigo.common.RestServiceGenerator;
import com.gsitm.hiwigo.login.LoginPresenter;
import com.gsitm.hiwigo.login.LoginPresenterImpl;
import com.gsitm.hiwigo.login.LoginView;

import static org.junit.Assert.*;

/**
 * 히위고 앱 단위테스트
 */
@RunWith(MockitoJUnitRunner.class)
public class HiwigoUnitTest {

    private LoginPresenter loginPresenter;

    @Mock
    LoginView loginview;
    @Mock
    Context context;

    @Before
    public void prepare() {
        loginPresenter = new LoginPresenterImpl(loginview, context);
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void isValidEmailTest() throws Exception {
        // TODO: 2017. 3. 15.  비동기 콜백에 대한 테스트를 어떻게 해야할지 모르겠음.
        /*
        assertEquals(false, loginPresenter.login("test", "test"));
        assertEquals(false, loginPresenter.isValidEmail("test@test"));
        assertEquals(false, loginPresenter.isValidEmail("te st@test.com"));
        assertEquals(false, loginPresenter.isValidEmail("te?st@test.com"));
        assertEquals(true, loginPresenter.isValidEmail("test@test.com"));
        */
    }


    @Test
    public void isLogedInTest() {
        assertEquals(true, loginPresenter.isLogin());
    }

    @Test
    public void messageTest() {

        RestServiceAgent restServiceAgent = RestServiceGenerator.createService(RestServiceAgent.class, null);
        Message message;

        //Call<message>

    }

}