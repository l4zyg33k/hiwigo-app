package com.gsitm.hiwigo;

import android.app.Activity;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.gsitm.hiwigo.login.LoginPresenter;
import com.gsitm.hiwigo.login.LoginPresenterImpl;
import com.gsitm.hiwigo.login.LoginView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest extends Activity {

    private LoginPresenter loginPresenter;
    private LoginView loginView;
    private Context context;

    @Before
    public void prepareTest() {
        loginPresenter = new LoginPresenterImpl(loginView, context);
    }

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.gsitm.hiwigo", appContext.getPackageName());
    }

    @Test
    public  void isLoginPossibleTest() {
        /*
        String token = loginPresenter.login("email", "password");
        assertEquals(true, !token.isEmpty());
        */
    }
}
